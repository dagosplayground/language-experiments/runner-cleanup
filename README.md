[[_TOC_]]

# GitLab Runner Cleanup 

Sometimes when you use a container runner... things get ugly. To clean up, you need an API key for user with owner or
maintainer access for the group/sub-group/project. 

## Installation 

Use a virtualenv and pip install the requirements file. You know the drill. 

## Use

Set the project number in the python script. Set the system environment variable `apikey` to the GitLab API key. 

Assuming the project number, names, and state match, the runners will be purged as much as pagination allows (about 200
runners pass). Can delete about 50k runners while listening to music in about 45 minutes. 



