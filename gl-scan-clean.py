import os
import gitlab
import time

GITLAB_URL = 'https://gitlab.com/'
PROJECT_NUMBER = "13597564"

class GitlabUser():

	def __init__(self,accessToken):
		"""Validates and accessed the gilablab user information from the API
		accessToken: User personal access token
		"""
		self.accessToken = accessToken
		self.auth = False

		self.gl = gitlab.Gitlab(GITLAB_URL,private_token=self.accessToken)

		self.errorMsg = str()

		self.authenticate()
		try:
			self.username = self.gl.user.username
			self.name = self.gl.user.name
			self.email = self.gl.user.email
		except AttributeError:
			self.username = str()
			self.name = str()
			self.email = str()
		except:
			raise

	def authenticate(self):
		"""Authenticates user"""
		try:
			if not self.auth:
				self.gl.auth()
				self.auth = True
		except gitlab.exceptions.GitlabAuthenticationError as e:
			# TODO: Do something here to show a problem
			self.errorMsg = e.error_message
			self.auth = False
		except:
			raise

	def groupRunnerList(self,group):
		"""Return a list runners to a group"""
		retval = list()
		try:
			group = self.gl.groups.get(group)
			retval = group.runners.list()
			return retval
		except Exception as e:
			raise

		return retval

	def runnerDelete(self,runnerId):
		"""Delete runner
		runnerId: Id of runner
		returns: None
		"""
		try:
			self.gl.runners.delete(runnerId)
		except:
			raise

	def runnerDetails(self,runnerId):
		try:
			return self.gl.runners.get(runnerId)
		except:
			raise

def main():
	"""Main testing of gitlab scan runners"""
	me = GitlabUser(os.getenv('apikey') )
	print(f"Me: {me.username}")


	page = 0
	# for runner in me.groupRunnerList("projectnumber"):
	for tst in me.groupRunnerList(PROJECT_NUMBER): # Loop through pages of runners 
		page += 1 
		print(f"Page {page}")
		# time.sleep(2)
		cleaned = True
		for runner in me.groupRunnerList(PROJECT_NUMBER):
			rDetail = me.runnerDetails(str(runner.id))
			# if rDetail.online == None and rDetail.tag_list[0] == 'k3s' and rDetail.status == 'not_connected':
			if rDetail.online == None:
				print(f"{rDetail}")
				me.runnerDelete(str(runner.id)) # Deletes runner
				cleaned = False
			# print(f"{rDetail.id}")
			# print(f"{rDetail}")
		if cleaned:
			break

if __name__ == "__main__":
	main()

